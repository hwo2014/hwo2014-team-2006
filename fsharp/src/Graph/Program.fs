﻿module Mtype.Vis

open System
open System.Drawing
open System.Windows.Forms

open Data
open Game

type private fu = delegate of unit -> unit

let inline private (!?) (x : decimal<_>) = float32 (decimal x)

type RaceMapView(raceData : RaceData) =
    inherit Control()

    let trackData = raceData.track

    let backColor = Color.Gainsboro
    let trackColor = Color.Black
    let trackPenWidth = 4.0f

    let viewScale = 0.5f

    let mutable carPositions : CarPosition seq option = None

    member this.CarPositions
        with get() = carPositions
        and set v =
            this.BeginInvoke(fu(fun () -> carPositions <- v; this.Invalidate())) |> ignore

    override this.OnPaint(ea) =
        base.OnPaint(ea)

        use g = ea.Graphics
        g.Clear(backColor)

        // clear
//        use bgBrush = new SolidBrush(backColor)
//        g.FillRectangle(bgBrush, this.ClientRectangle)


        // init the turtle
//        g.TranslateTransform(float32 this.Size.Width / 2.0f, float32 this.Size.Height / 2.0f)
        g.TranslateTransform(500.0f, 500.0f)

        g.ScaleTransform(viewScale, viewScale) // ?
        g.TranslateTransform(
            !? trackData.startingPoint.position.x,
            !? trackData.startingPoint.position.y)
        g.RotateTransform(!? trackData.startingPoint.angle)

        use trackBrush = new SolidBrush(trackColor)
        use trackPen = new Pen(trackBrush, trackPenWidth)

        let degToRad (x : decimal<deg>) : float32 =
            (float <| decimal x) * Math.PI / 180.0
            |> float32
            //|> LanguagePrimitives.Float32WithMeasure
        
        // iterate over pieces
        for piece in trackData.pieces do
            match piece.Geometry with
            | Straight len ->
                g.DrawLine(trackPen, 0.0f, 0.0f, !? len, 0.0f)
                g.TranslateTransform(!? len, 0.0f)
            | Turn { radius = r; angle = a } ->
                g.DrawArc(trackPen, -(!? r), 0.0f, (!? r) * 2.0f, (!? r) * 2.0f, -90.0f, (!? a))
                g.TranslateTransform(
                    cos (degToRad (a - 90.0m<deg>)) * (!? r),
                    (!? r) + sin (degToRad (a - 90.0m<deg>)) * (!? r))
                g.RotateTransform(!? a)

[<EntryPoint; STAThread>]
let main args =
    let LOG = Log.log "(( ma!n ))"

//    let host = "hakkinen.helloworldopen.com"
//    let port = "8091"
//    let botKey = "gevzs6N46IKqiA"
//    let trackName = "keimola"
//
//    let game =
//        async {
//            LOG.Info "MODE: select track"
//            let gs = new GameStarter(host, port)
//            let! game = gs.JoinRace({ name = Namegen.newName(); key = botKey}, trackName, null, 1)
//
//            return game
//        } |> Async.RunSynchronously

    use f = new Form()

    let track = Tracks.germany

    let v = new RaceMapView({ track = track; cars = [| |]; raceSession = { laps = 0; maxLapTimeMs = 0<ms>; quickRace = true } })
    v.Dock <- DockStyle.Fill
    f.Controls.Add(v)
//    f.Load.Add(fun _ -> game.Play() |> Async.Start)
    Application.Run f

    0
