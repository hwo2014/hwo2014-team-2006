﻿module Mtype.Tests

open System
open Xunit

open Data

type DataTests() =
    [<Fact>]
    let ``lap finished message is parsed correctly`` () =
        let (LapFinished d) =
            """{"msgType": "lapFinished", "data": {
              "car": {
                "name": "Schumacher",
                "color": "red"
              },
              "lapTime": {
                "lap": 1,
                "ticks": 666,
                "millis": 6660
              },
              "raceTime": {
                "laps": 1,
                "ticks": 666,
                "millis": 6660
              },
              "ranking": {
                "overall": 1,
                "fastestLap": 1
              }
            }, "gameId": "OIUHGERJWEOI", "gameTick": 300}"""
            |> ServerMessage.FromJson

        Assert.Equal<string>("OIUHGERJWEOI", d.gameId)
        Assert.Equal(300<tick>, d.gameTick)
        Assert.Equal({ name = "Schumacher"; color = "red" }, d.data.car)
        Assert.Equal({ lap = 1; ticks = 666<tick>; millis = 6660<ms> }, d.data.lapTime)

    [<Fact>]
    let ``game init is parsed correctly`` () =
        let (GameInit { race = race }) =
            """{"msgType":"gameInit","data":{"race":{"track":{"id":"keimola","name":"Keimola","pieces":[{"length":100.0},{"length":100.0},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5,"switch":true},{"length":100.0},{"length":100.0},{"radius":200,"angle":-22.5},{"length":100.0},{"length":100.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"radius":100,"angle":-45.0},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":200,"angle":22.5},{"radius":200,"angle":-22.5},{"length":100.0,"switch":true},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":62.0},{"radius":100,"angle":-45.0,"switch":true},{"radius":100,"angle":-45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"radius":100,"angle":45.0},{"length":100.0,"switch":true},{"length":100.0},{"length":100.0},{"length":100.0},{"length":90.0}],"lanes":[{"distanceFromCenter":-10,"index":0},{"distanceFromCenter":10,"index":1}],"startingPoint":{"position":{"x":-300.0,"y":-44.0},"angle":90.0}},"cars":[{"id":{"name":"e76580e5-b025-4","color":"red"},"dimensions":{"length":40.0,"width":20.0,"guideFlagPosition":10.0}}],"raceSession":{"laps":3,"maxLapTimeMs":60000,"quickRace":true}}},"gameId":"4ab906c4-de82-4d7c-bc39-58270156eb3c"}"""
            |> ServerMessage.FromJson

        Assert.Equal<string>("keimola", race.track.id)
        Assert.Equal(
            { CanSwitch = false; Geometry = Straight 100.0m<cm> },
            race.track.pieces.[0])
        Assert.Equal(
            { CanSwitch = true; Geometry = Straight 100.0m<cm> },
            race.track.pieces.[3])
        Assert.Equal(
            { CanSwitch = true; Geometry = Turn { radius = 200.0m<cm>; angle = 22.5m<deg> } },
            race.track.pieces.[8])
