﻿module Mtype.CLI

open System

open Data
open Network
open Game

[<EntryPoint>]
let main args =
    let LOG = Log.log "(( ma!n ))"

    match args with
    | [| host; port; botName; botKey |] ->
        async {
            let gs = new GameStarter(host, port)
            let! game = gs.SimpleJoin({ name = botName; key = botKey})
            do! game.Play()
        } |> Async.RunSynchronously

    | [| "-x"; host; port; botKey; "-track"; trackName |] ->
        async {
            LOG.Info "MODE: select track"
            let gs = new GameStarter(host, port)
            let! game = gs.JoinRace({ name = Namegen.newName(); key = botKey}, trackName, null, 1)

            do! game.Play()
        } |> Async.RunSynchronously
    | [| "-x"; host; port; botKey; "-create"; trackName; carCountString; password |] ->
        async {
            LOG.Info "MODE: host"
            let gs = new GameStarter(host, port)
            let carCount = Convert.ToInt32 carCountString

            LOG.Info "creating race host..."
            let! host =
                gs.CreateRace(
                    { name = Namegen.newName(); key = botKey},
                    trackName, password, carCount)

            do! host.Play()
        } |> Async.RunSynchronously
    | [| "-x"; host; port; botKey; "-join"; trackName; carCountString; password |] ->
        async {
            LOG.Info "MODE: join"
            let gs = new GameStarter(host, port)
            let carCount = Convert.ToInt32 carCountString

            LOG.Info "joining race..."
            let! game =
                gs.JoinRace(
                    { name = Namegen.newName(); key = botKey},
                    trackName, password, carCount)

            do! game.Play()
        } |> Async.RunSynchronously
    | [| "-x"; host; port; botKey; "-multi"; trackName; carCountString |] ->
        async {
            LOG.Info "MODE: multiplayer"
            let gs = new GameStarter(host, port)
            let carCount = Convert.ToInt32 carCountString
            let password = System.Guid.NewGuid().ToString()

            let! host = async {
                LOG.Info "creating race host..."
                return! gs.CreateRace(
                    { name = Namegen.newName(); key = botKey},
                    trackName, password, carCount)
            }

            let! clients =
                seq {
                    for i in 1 .. carCount do
                        yield async {
                            LOG.Info "joining race..."
                            return! gs.JoinRace(
                                { name = Namegen.newName(); key = botKey},
                                trackName, password, carCount)
                        }
                } |> Async.Parallel

            do!
                (host :: (List.ofArray clients))
                |> Seq.map (fun x -> x.Play())
                |> Async.Parallel
                |> Async.Ignore
        } |> Async.RunSynchronously

    0
