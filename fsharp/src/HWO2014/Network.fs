﻿module Mtype.Network

open System
open System.IO
open System.Net.Sockets

open Data

// @TODO each message should be handled within a tick and response has to be sent, otherwise
// the server will d/c us. it should be ensured that each incoming message is handled somehow
// within the time frame.
type Client(host, portString : string) =
    let log = Log.log "TCP"
    
    let client = new TcpClient(host, Convert.ToInt32 portString)
    let stream = client.GetStream()
    let reader = new StreamReader(stream)
    let writer = new StreamWriter(stream)

    member this.ReadMessage() = async {
        match reader.ReadLine() with
        | null ->
            log.Trace "<<-- |||"
            return None
        | x ->
            log.Trace "<<-- %s" x
            return Some <| ServerMessage.FromJson x
    }

    member this.SendMessage(msg : ClientMessage) = async {
        let x = msg.ToJson()
        log.Trace "=>>> %s" x
        writer.WriteLine(x)
        writer.Flush()
    }

    interface IDisposable with
        member this.Dispose() =
            (client :> IDisposable).Dispose()
