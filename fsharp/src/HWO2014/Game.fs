﻿module Mtype.Game

open System

open Network
open Data

type BotResponseAction =
    | Nothing
    | SendThrottle of decimal
    | SendSwitchLane of SwitchDirection

type BotAI =
    abstract SimulationTick : int<tick> * CarPosition[] -> Async<BotResponseAction>

module Bot1 =
    type private ServerToBotMsg =
        | GameOver
        | SimulationTick of int<tick> * CarPosition[] * AsyncReplyChannel<BotResponseAction>

    type BotAI1(raceData : RaceData, thisCar : CarId) as this=
        let LOG = Log.log <| sprintf "botAI-1 %d" (this.GetHashCode())

        let pow x y = exp (log x * y)

        let agent = MailboxProcessor<_>.Start <| fun a ->
            let rec loop () = async {
                let! m = a.Receive()

                match m with
                | GameOver -> return ()
                | SimulationTick (tick, carPositions, chan) ->
                    let thisPos =
                        carPositions
                        |> Array.find (fun x -> x.id = thisCar)

                    let gameTimeSec = double tick / 60.0<s/tick>

                    let newThrottle =
                        match decimal thisPos.angle |> float |> LanguagePrimitives.FloatWithMeasure |> abs with
                        | a when a <= 20.0<deg> -> decimal <| 1.0 - (pow (a / 20.0<deg>) 0.3) * 0.9
                        | _ -> 0.1m

                    LOG.Debug "time = %A, angle = %A, newThrottle = %A" gameTimeSec thisPos.angle newThrottle
                    chan.Reply(SendThrottle newThrottle)
                    return! loop ()
            }
            loop ()

        interface BotAI with
            member this.SimulationTick(time : int<tick>, carPositions : CarPosition[]) =
                agent.PostAndAsyncReply(fun chan -> SimulationTick (time, carPositions, chan))

type Game(conn : Client, botId : BotId, carId : CarId, race : RaceData, ai : BotAI) as this =
    let LOG = Log.log <| sprintf "gaym %d" (this.GetHashCode())

    member this.Race with get() = race

    member this.Play() = async {
        let! (Some (CarPositions { data = { Positions = initialCarPositions }})) = conn.ReadMessage()
        do! conn.SendMessage Ping
        LOG.Info "initial car positions"

        let! (Some (GameStart _)) = conn.ReadMessage()
        do! conn.SendMessage Ping
        LOG.Info "GAME START"

        let processSimulationTick tick positions = async {
            let! r = ai.SimulationTick(tick, positions)
            match r with
            | Nothing -> return Ping
            | SendThrottle t -> return Throttle { GameTick = tick; Data = t }
            | SendSwitchLane d -> return SwitchLane { GameTick = tick; Data = d }
        }

        let rec loop () = async {
            try
                let! m = conn.ReadMessage()

                try
                    match m with
                    | None -> LOG.Info "server disconnected"
                    | Some (GameStart { gameTick = tick }) ->
                        let! m = processSimulationTick tick initialCarPositions
                        do! conn.SendMessage m
                        return! loop ()
                    | Some (CarPositions { gameTick = tick; data = { Positions = positions } }) ->
                        let! m = processSimulationTick tick positions
                        do! conn.SendMessage m
                        return! loop ()
                    | Some (GameEnd x) ->
                        LOG.Info "game END: %A" x
                    | other ->
                        LOG.Error "unhandled message: %A" other
                        do! conn.SendMessage Ping
                        return! loop ()
                with ex ->
                    LOG.ErrorException ex "FAILED to process server message"
                    do! conn.SendMessage Ping
                    return! loop ()
            with ex ->
                LOG.ErrorException ex "FAILED to read server message"
                return! loop ()
        }

        return! loop ()
    }

type GameStarter(host, port) =
    let LOG = Log.log "mgr"

    let createGame (conn : Client) botId = async {
//        let! (Some (YourCar carId)) = conn.ReadMessage()
        let! yourCar = conn.ReadMessage()
        let (Some (YourCar carId)) = yourCar
        LOG.Info "my car: %A" carId

        let! (Some (GameInit { race = race })) = conn.ReadMessage()
        LOG.Info "game init, %d cars, %A" (race.cars.Length) race.raceSession

        let bot = new Bot1.BotAI1(race, carId)

        return new Game(conn, botId, carId, race, bot)
    }

    member this.CreateRace(botId, trackName, password, carCount) = async {
        let conn = new Client(host, port)
        do! conn.SendMessage <| CreateRace {
            botId = botId
            trackName = trackName
            password = password
            carCount = carCount
        }
        let! (Some ack) = conn.ReadMessage()
        do! conn.SendMessage Ping
        LOG.Info "createrace ack: %A" ack

        return! createGame conn botId
    }

    member this.JoinRace(botId, trackName, password, carCount) = async {
        let conn = new Client(host, port)
        do! conn.SendMessage <| JoinRace {
            botId = botId
            trackName = trackName
            password = password
            carCount = carCount
        }
        let! (Some ack) = conn.ReadMessage()
        LOG.Info "JOINrace ack: %A" ack

        return! createGame conn botId
    }

    member this.SimpleJoin(botId) = async {
        let conn = new Client(host, port)
        do! conn.SendMessage <| Join botId
        let! (Some ack) = conn.ReadMessage()
        LOG.Info "join ack: %A" ack

        return! createGame conn botId
    }
